# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3 

class BlendWithEvent(Event3):
    NAME = "blend-with"

    def perform(self):
        if not self.object.has_prop("blendable") or not self.object2.has_prop("blender"):
            self.fail()
            return self.inform("blend-with.failed")
        if self.object.is_empty():
            self.add_prop("object-is-empty")
        if self.object.sup1():
            self.add_prop("object-sup-one")
        self.inform("blend-with")
