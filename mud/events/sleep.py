# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class SleepEvent(Event2):
	NAME ="sleep"
	
	def perform(self):
		return self.inform("sleep.failed")
		
class SleepWithEvent(Event3):
    NAME = "sleep-with"

    def perform(self):
        if not self.object.has_prop("sleepable") or not self.object2.has_prop("somnifere"):
            self.fail()
            return self.inform("sleep-with.failed")
        self.inform("sleep-with")
