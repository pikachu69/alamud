# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import BreakEvent, BreakWithEvent

class BreakAction(Action2):
    EVENT = BreakEvent
    RESOLVE_OBJECT = "resolve_for_use"
    ACTION = "break"

class BreakWithAction(Action3):
    EVENT = BreakWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "break-with"
